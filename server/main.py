import time
import BaseHTTPServer
import argparse
import urlparse

import match

HOST_NAME = 'localhost'
#HOST_NAME = '192.168.1.59'
PORT_NUMBER = 80

match = match.Match()

class MyHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_HEAD(self):
        self.send_response(200)
        self.send_header("Content-Type", "text/html")
        self.end_headers()
    def do_GET(self):
        """Respond to a GET request."""
        self.send_response(200)
        self.send_header("Content-type", "text/html")
        self.end_headers()
        self.wfile.write("<html><head><title>Title goes here.</title></head>")
        self.wfile.write("<body><p>This is a test.</p>")
        # If someone went to "http://something.somewhere.net/foo/bar/",
        # then s.path equals "/foo/bar/".
        self.wfile.write("<p>You accessed path: %s</p>" % s.path)
        self.wfile.write("</body></html>")
    def do_POST(self):
        contentLength = int(self.headers.getheader("Content-Length"))
        self.send_response(200)
        self.send_header("Content-type", "text/json")
        self.end_headers()
        body = urlparse.parse_qs(self.rfile.read(contentLength))
        if body["type"][0] == "command":
            command = body["json"][0]
            response = match.handleCommand(command)
            self.wfile.write(response)

if __name__ == '__main__':
    # Parse command line parameters.
    parser = argparse.ArgumentParser(description='Process host name.')
    parser.add_argument('--host', metavar='host', type=str, nargs=1, help='Host name or ip address of the host. The default value is "localhost".')
    parser.add_argument('--port', metavar='port', type=int, nargs=1, help='Port number. The default value is 80.')
    args = parser.parse_args()
    if args.host:
        HOST_NAME = args.host[0]
    if args.port:
        PORT_NUMBER = args.port[0]
    # Run server.
    server_class = BaseHTTPServer.HTTPServer
    httpd = server_class((HOST_NAME, PORT_NUMBER), MyHandler)
    print time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER)