import json
import random
import copy
import player

class Match:

    def __init__(self, sid, emit):
        self.sid = sid
        self.emit = emit

        self.allCards = [{'attack': 3, 'health': 5, 'mana':2}, {'attack': 2, 'health': 2, 'mana':1}, {'attack': 5, 'health': 10, 'mana':4}]

        self.maxCardsInHandsCount = 4;

        self.player = player.Player()
        self.enemy = player.Player()

        self.enemyTableCards = [None, None, None]
        self.enemyTableCardsCount = 0

    def handleCommand(self, command):
        print command
        parsed = json.loads(command)
        print parsed["command"]
        if parsed["command"] == "StartGameCmd":
            return self.startGameCmd(parsed)
        elif parsed["command"] == "EndTurnCmd":
            return self.endTurnCmd(parsed)
        elif parsed["command"] == "PlayCardCmd":
            return self.playCardCmd(parsed)
        elif parsed["command"] == "EchoCmd":
            return command
        else:
            return "unknown command"

    def startGameCmd(self, command):
    	self.player.mana = 1
    	self.player.maxMana = 1
        self.player.health = 20
        self.player.deck = [{'id':0, 'tplId':0}, {'id':1, 'tplId':0}, {'id':2, 'tplId':0}, {'id':3, 'tplId':0}, 
                            {'id':4, 'tplId':1}, {'id':5, 'tplId':1}, {'id':6, 'tplId':1}, {'id':7, 'tplId':1},
                            {'id':8, 'tplId':2}, {'id':9, 'tplId':2}, {'id':10, 'tplId':2}, {'id':10, 'tplId':2}]
        self.player.cardsInHandsCount = self.maxCardsInHandsCount
        self.player.tableCards = [None, None, None]
        self.player.cardsInHands = []

        self.enemy.health = 20
        self.enemy.tableCards  = [None, None, None]

        cmd = {'commands': [{'type': 'UpdateParams', 'params': {'mana': self.player.mana, 'maxMana': self.player.maxMana, 'health' : self.player.health, 'enemyHealth': self.enemy.health}}]}
        self.emit(self.sid, json.dumps(cmd))
        
        # GetCardInHands commands
        commandsList = []
        for i in range(0, self.maxCardsInHandsCount):
            print "command GetCardInHands"
            num = random.randint(0, len(self.player.deck) - 1)
            cardInDeck = self.player.deck[num]
            card = copy.deepcopy(self.allCards[cardInDeck['tplId']])
            card.update({'id':cardInDeck['id']})
            commandsList.append({'type': 'GetCardInHands', 'card': card})
            self.player.cardsInHands.append(cardInDeck)
            self.player.deck.remove(cardInDeck)
        print "self.player.cardsInHands ", self.player.cardsInHands

        if len(commandsList) > 0:
            cmd = {'commands': commandsList}
            self.emit(self.sid, json.dumps(cmd))

    def endTurnCmd(self, command):
        self.player.maxMana += 1
        self.player.mana = self.player.maxMana

        # player cards attack commands
        i = 0
        enemyDead = False
        for tableCard in self.player.tableCards:
            if tableCard != None and not enemyDead:
                print "command CardAttack"
                damage = tableCard['attack']
                cmd = {'commands': [{'type': 'CardAttack', 'slot': i, 'damage': damage}]}
                self.emit(self.sid, json.dumps(cmd))
                if self.enemy.tableCards[i] != None:
                    self.enemy.tableCards[i]['health'] -= damage
                    if self.enemy.tableCards[i]['health'] <= 0:
                        self.enemy.tableCards[i] = None
                else:
                    self.enemy.health -= damage
                    if self.enemy.health <= 0:
                        self.enemy.health = 0
                        cmd = {'commands': [{'type': 'MatchEnd', 'status': 'win'}]}
                        self.emit(self.sid, json.dumps(cmd))
                        enemyDead = True
                    cmd = {'commands': [{'type': 'UpdateParams', 'params': {'enemyHealth': self.enemy.health}}]}
                    self.emit(self.sid, json.dumps(cmd))
            i += 1

        # ENEMY TURN

        # EnemyPlayCard command
        emptySlotsIndexes = [x for x, y in enumerate(self.enemy.tableCards) if y == None]
        if (len(emptySlotsIndexes) > 0):
            print "command EnemyPlayCard"
            enemyCard = {'attack': 4, 'health': 6, 'mana':4}
            slotIndex = emptySlotsIndexes[0]
            cmd = {'commands': [{'type': 'EnemyPlayCard', 'card': enemyCard, 'slot': slotIndex}]}
            self.emit(self.sid, json.dumps(cmd))
            self.enemy.tableCards[slotIndex] = enemyCard

        # enemy attack commands
        i = 0
        playerDead = False
        for tableCard in self.enemy.tableCards:
            if tableCard != None and not playerDead:
                print "command enemy CardAttack"
                damage = tableCard['attack']
                cmd = {'commands': [{'type': 'EnemyCardAttack', 'slot': i, 'damage': damage}]}
                self.emit(self.sid, json.dumps(cmd))
                if self.player.tableCards[i] != None:
                    self.player.tableCards[i]['health'] -= damage
                    if self.player.tableCards[i]['health'] <= 0:
                        self.player.tableCards[i] = None
                else:
                    self.player.health -= damage
                    if self.player.health <= 0:
                        self.player.health = 0
                        cmd = {'commands': [{'type': 'MatchEnd', 'status': 'lose'}]}
                        self.emit(self.sid, json.dumps(cmd))
                        playerDead = True
                    cmd = {'commands': [{'type': 'UpdateParams', 'params': {'health': self.player.health}}]}
                    self.emit(self.sid, json.dumps(cmd))
            i += 1

        # GetCardInHands command
        if self.player.cardsInHandsCount < self.maxCardsInHandsCount and len(self.player.deck) > 0:
            print "command GetCardInHands"
            num = random.randint(0, len(self.player.deck) - 1)
            cardInDeck = self.player.deck[num]
            card = copy.deepcopy(self.allCards[cardInDeck['tplId']])
            card.update({'id':cardInDeck['id']})
            print "card ", card
            cmd = {'commands': [{'type': 'GetCardInHands', 'card': card}]}
            self.emit(self.sid, json.dumps(cmd))
            self.player.cardsInHands.append(cardInDeck)
            self.player.deck.remove(cardInDeck)
            self.player.cardsInHandsCount += 1

        # update mana
        cmd = {'commands': [{'type': 'UpdateParams', 'params': {'mana': self.player.mana, 'maxMana': self.player.maxMana}}]}
        self.emit(self.sid, json.dumps(cmd))

    def playCardCmd(self, command):
        slot = command["slot"]
        cardId = command["cardId"]
        print "cardId ", cardId
        print "self.player.cardsInHands ", self.player.cardsInHands
        cardInHands = next((x for x in self.player.cardsInHands if x['id'] == cardId), None)
        if self.player.tableCards[slot] == None and cardInHands != None:
            self.player.tableCards[slot] = copy.deepcopy(self.allCards[cardInHands['tplId']])
            self.player.mana -= self.player.tableCards[slot]['mana']
            self.player.cardsInHandsCount -= 1
            cmd = {'commands': [{'type': 'UpdateParams', 'params': {'mana': self.player.mana}}]}
            self.emit(self.sid, json.dumps(cmd))
        else:
            self.emit(self.sid, json.dumps({ 'status':'fail'}))