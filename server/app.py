import match
import json

# set this to 'threading', 'eventlet', or 'gevent'
async_mode = 'eventlet'

if async_mode == 'eventlet':
    import eventlet
    eventlet.monkey_patch()
elif async_mode == 'gevent':
    from gevent import monkey
    monkey.patch_all()

import time
from threading import Thread
from flask import Flask, render_template
import socketio

sio = socketio.Server(logger=True, async_mode=async_mode)
app = Flask(__name__)
app.wsgi_app = socketio.Middleware(sio, app.wsgi_app)
app.config['SECRET_KEY'] = 'secret!'
thread = None

matches = {}

"""
def background_thread():
    count = 0
    while True:
        time.sleep(10)
        count += 1
        sio.emit('my response', {'data': 'Server generated event'}, namespace='/')
"""

@app.route('/')
def index():
    """
    global thread
    if thread is None:
        thread = Thread(target=background_thread)
        thread.start()
    """
    #return render_template('index.html')
    return 'wtf'

@sio.on('command', namespace='/')
def handle_message(sid, command):
    print('received message')
    global matches
    #global myMatch
    commandStr = json.dumps(command)
    print('received command: ' + commandStr)
    hmatch = matches.get(sid, None)
    if hmatch != None:
        hmatch.handleCommand(commandStr)
        #response = hmatch.handleCommand(commandStr)
        #print('response: ' + response)
        #sio.emit('commandnet', json.loads(response), room=sid, namespace='/')
    else:
        print('match with this sid not found: ' + sid)

def emit(sid, command):
    sio.emit('commandnet', json.loads(command), room=sid, namespace='/')

"""
@sio.on('my event', namespace='/')
def test_message(sid, message):
    sio.emit('my response', {'data': message['data']}, room=sid, namespace='/')

@sio.on('my broadcast event', namespace='/')
def test_broadcast_message(sid, message):
    sio.emit('my response', {'data': message['data']}, namespace='/')


@sio.on('join', namespace='/')
def join(sid, message):
    sio.enter_room(sid, message['room'], namespace='/')
    sio.emit('my response', {'data': 'Entered room: ' + message['room']},
             room=sid, namespace='/')


@sio.on('leave', namespace='/')
def leave(sid, message):
    sio.leave_room(sid, message['room'], namespace='/')
    sio.emit('my response', {'data': 'Left room: ' + message['room']},
             room=sid, namespace='/')


@sio.on('close room', namespace='/')
def close(sid, message):
    sio.emit('my response',
             {'data': 'Room ' + message['room'] + ' is closing.'},
             room=message['room'], namespace='/')
    sio.close_room(message['room'], namespace='/')


@sio.on('my room event', namespace='/')
def send_room_message(sid, message):
    sio.emit('my response', {'data': message['data']}, room=message['room'],
             namespace='/')
"""

@sio.on('disconnect request', namespace='/')
def disconnect_request(sid):
    sio.disconnect(sid, namespace='/')


@sio.on('connect', namespace='/')
def client_connect(sid, environ):
    global matches
    print('Client connected: ' + sid)
    matches[sid] = match.Match(sid, emit)


@sio.on('disconnect', namespace='/')
def client_disconnect(sid):
    global matches
    print('Client disconnected: ' + sid)
    del matches[sid]


if __name__ == '__main__':
    if async_mode == 'threading':
        # deploy with Werkzeug
        app.run(threaded=True)
    elif async_mode == 'eventlet':
        # deploy with eventlet
        import eventlet
        eventlet.wsgi.server(eventlet.listen(('', 5000)), app)
    elif async_mode == 'gevent':
        # deploy with gevent
        from gevent import pywsgi
        try:
            from geventwebsocket.handler import WebSocketHandler
            websocket = True
        except ImportError:
            websocket = False
        if websocket:
            pywsgi.WSGIServer(('', 5000), app,
                              handler_class=WebSocketHandler).serve_forever()
        else:
            pywsgi.WSGIServer(('', 5000), app).serve_forever()
    else:
        print('Unknown async_mode: ' + async_mode)
