﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class CoroutineTaskManager : MonoBehaviour
{
	private static CoroutineTaskManager _instance = null;
	public static CoroutineTaskManager Get
	{
		get
		{
			if (_instance == null) {
				_instance = FindObjectOfType(typeof(CoroutineTaskManager)) as CoroutineTaskManager;
				return _instance;
			}
			else {
				return _instance;
			}
		}
	}

	private List<IEnumerator> tasks = new List<IEnumerator>();

	void Awake()
	{
		StartCoroutine(DoTask());
	}

	IEnumerator DoTask()
	{
		while(true) {
			if (tasks.Count == 0) {
				yield return null;
			} else {
				IEnumerator task = tasks[0];
				yield return StartCoroutine(task);
				tasks.Remove(task);
			}
		}
	}

	public void AddTask(IEnumerator task)
	{
		tasks.Add(task);
	}
}
