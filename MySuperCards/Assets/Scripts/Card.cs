﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Card
{
	public int id;
	public int attack;
	public int health;
	public int mana;

	public Card(int id, int attack, int health, int mana)
	{
		this.id = id;
		this.attack = attack;
		this.health = health;
		this.mana = mana;
	}
}
