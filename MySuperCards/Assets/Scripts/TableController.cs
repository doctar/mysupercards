﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class TableController : MonoBehaviour 
{
	public List<Slot> slots;
	public Dictionary<Slot, CardController> cards;

	private static TableController _instance = null;
	public static TableController Get
	{
		get
		{
			if (_instance == null) {
				_instance = FindObjectOfType(typeof(TableController)) as TableController;
				return _instance;
			}
			else {
				return _instance;
			}
		}
	}

	void Awake()
	{
		cards = new Dictionary<Slot, CardController>();
		foreach (var slot in slots) {
			slot.onClick += SlotClicked;
		}
		HandController.Get.onPlayCard += OnPlayCardOnSlot;
	}

	void SlotClicked(Slot slot)
	{
		HandController.Get.TryPlaySelectedCardOnSlot(slot);
	}

	void OnPlayCardOnSlot(Slot slot, CardController cardView)
	{
		if (cardView != null) {
			cardView.transform.DOMove(slot.transform.position, 0.4f);
			cards.Add(slot, cardView);

			int slotIndex = slots.FindIndex( x => x == slot );
			new PlayCardCmd(slotIndex, cardView.card.id).Send();
		}
	}

	public CardController GetCardControllerInSlot(int slotIndex)
	{
		if (slots.Count > slotIndex) {
			if (cards.ContainsKey(slots[slotIndex])) {
				return cards[slots[slotIndex]];
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public Slot GetSlotByIndex(int slotIndex)
	{
		if (slots.Count > slotIndex) {
			return slots[slotIndex];
		} else {
			return null;
		}
	}

	public void DestroyCard(int slotIndex)
	{
		Slot slot = slots[slotIndex];
		if (cards.ContainsKey(slot)) {
			CardController card = cards[slot];
			cards.Remove(slot);
			Destroy(card.gameObject);
		}
	}
}
