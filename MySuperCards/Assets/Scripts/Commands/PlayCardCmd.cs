﻿using UnityEngine;
using System.Collections;

public class PlayCardCmd : Command
{
	public int slot;
	public int cardId;

	public PlayCardCmd(int slot, int cardId)
	{
		this.slot = slot;
		this.cardId = cardId;
	}
}