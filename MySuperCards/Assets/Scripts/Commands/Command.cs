﻿using UnityEngine;
using System;
using System.Collections;

public class Command
{
	public string command;

	public virtual void Send(Action<string> callback = null)
	{
		command = this.GetType ().Name;
		string sendParams = JsonUtility.ToJson(this);

		Debug.Log("Comand send: " + sendParams);
		Network.Get.Request(sendParams);
		//Network.Get.RequestWWW(sendParams, callback);
	}
}
