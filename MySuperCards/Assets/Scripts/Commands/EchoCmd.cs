﻿using UnityEngine;
using System.Collections;

public class EchoCmd : Command
{
	public string echoStr;

	public EchoCmd(string echo)
	{
		echoStr = echo;
	}
}