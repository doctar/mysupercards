﻿using UnityEngine;
using System;
using System.Collections;
using SocketIO;

public class Network
{
	private static Network instance;
	public static Network Get
	{
		get {
			if (instance == null) {
				instance = new Network ();
			}
			return instance;
		}
	}


	public string url = "http://localhost/";
	//public string url = "http://192.168.1.59/";

	public Action<string> onReqestCallback;

	private SocketIOComponent socket;
	public void Init()
	{
		GameObject go = GameObject.Find("SocketIO");
		socket = go.GetComponent<SocketIOComponent>();


		socket.On("commandnet", CommandNet);
	}

	public void CommandNet(SocketIOEvent e)
	{
		string str = e.data.ToString();
		Debug.Log("socketIO Ok: " + str);
		if (onReqestCallback != null) {
			onReqestCallback(str);
		}
	}

	public void Request(string param) 
	{
		JSONObject jobj = JSONObject.Create(param);
		socket.Emit("command", jobj);
	}

	public void RequestWWW(string postParams, Action<string> callback = null) 
	{
		WWWForm form = new WWWForm();
		form.AddField("type", "command");
		form.AddField("json", postParams);
		WWW www = new WWW(url, form);
		GameManager.Get.StartCoroutine(WaitForRequest(www, callback));
	}

	IEnumerator WaitForRequest(WWW www, Action<string> callback)
	{
		yield return www;
		// check for errors
		if (www.error == null)
		{
			Debug.Log("www Ok: " + www.text);
			if (onReqestCallback != null) {
				onReqestCallback(www.text);
			}
			if (callback != null)
			{
				callback(www.text);
			}
		} else {
			Debug.Log("www Error: "+ www.error);
		}    
	}  
}
