﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayersController : MonoBehaviour 
{
	private static PlayersController _instance = null;
	public static PlayersController Get
	{
		get
		{
			if (_instance == null) {
				_instance = FindObjectOfType(typeof(PlayersController)) as PlayersController;
				return _instance;
			}
			else {
				return _instance;
			}
		}
	}

	private int _mana;
	private int _maxMana;
	public int mana
	{
		get { return _mana;}
		set { _mana = value; UpdateTextMana();}
	}
	public int maxMana
	{
		get { return _maxMana;}
		set { _maxMana = value; UpdateTextMana();}
	}
	public Text manaText;

	private int _hp;
	public int hp
	{
		get { return _hp;}
		set { _hp = value; UpdateTextHp();}
	}
	public Text hpText;

	private int _enemyHp;
	public int enemyHp
	{
		get { return _enemyHp;}
		set {
				_enemyHp = value;
				if (enemyHpText != null) {
					enemyHpText.text = string.Format ("{0}", _enemyHp.ToString ());
				}
			}
	}
	public Text enemyHpText;

	private void UpdateTextMana()
	{
		if (manaText != null) {
			manaText.text = string.Format("{0}/{1}", mana.ToString(), maxMana.ToString());
		}
	}

	private void UpdateTextHp()
	{
		if (hpText != null) {
			hpText.text = string.Format("{0}", hp.ToString());
		}
	}

	public IEnumerator UpdateParams(string str)
	{
		JSONObject update = new JSONObject(str);
		if (update["mana"] != null) {
			mana = (int)update["mana"].n;
		}
		if (update["maxMana"]) {
			maxMana = (int)update["maxMana"].n;
		}
		if (update["health"]) {
			hp = (int)update["health"].n;
		}
		if (update["enemyHealth"]) {
			enemyHp = (int)update["enemyHealth"].n;
		}
		yield return null;
	}

	public bool CanSpendMana(int m)
	{
		if (mana >= m) {
			return true;
		} else {
			return false;
		}
	}
}
