﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class HandController : MonoBehaviour 
{
	public List<GameObject> cardViewHolder;
	public GameObject cardPrefab;

	public Action<Slot, CardController> onPlayCard;

	private List<CardController> cardViews;
	private CardController selectedCard;

	private int orderCounter = 0;

	private static HandController _instance = null;
	public static HandController Get
	{
		get
		{
			if (_instance == null) {
				_instance = FindObjectOfType(typeof(HandController)) as HandController;
				return _instance;
			}
			else {
				return _instance;
			}
		}
	}

	void Awake()
	{
		cardViews = new List<CardController>();
	}

	public IEnumerator PutCardInHands(Card card)
	{
		GameObject cardGo = GameObject.Instantiate(cardPrefab, new Vector3(-4f, -2f, 0f), cardViewHolder[cardViews.Count].transform.rotation) as GameObject;
		CardController cardView = cardGo.GetComponent<CardController>();
		cardView.Init(card, orderCounter);
		orderCounter++;
		Tween tween = cardGo.transform.DOMove(cardViewHolder[cardViews.Count].transform.position, 0.4f);
		yield return tween.WaitForCompletion();
		cardView.onClick += OnCardClicked;
		cardViews.Add(cardView);
	}

	void OnCardClicked(CardController cardView)
	{
		foreach (var cv in cardViews) {
			cv.SetSelected(false);
		}
		cardView.SetSelected(true);
		selectedCard = cardView;
	}

	public int GetCardCount()
	{
		return cardViews.Count;
	}

	public CardController GetSelectedCard()
	{
		return selectedCard;
	}

	public void TryPlaySelectedCardOnSlot(Slot slot)
	{
		if (selectedCard != null && onPlayCard != null) {
			selectedCard.SetSelected(false);
			if (PlayersController.Get.CanSpendMana(selectedCard.card.mana)) {
				onPlayCard(slot, selectedCard);
				cardViews.Remove(selectedCard);
				selectedCard.onClick -= OnCardClicked;
				selectedCard = null;
				ResortCards();
			}
		}
	}

	private void ResortCards()
	{
		int i = 0;
		foreach (var cv in cardViews) {
			//cv.transform.position = cardViewHolder[i].transform.position;
			cv.transform.DOMove(cardViewHolder[i].transform.position, 0.4f);
			cv.transform.rotation = cardViewHolder[i].transform.rotation;
			i++;
		}
	}
}
