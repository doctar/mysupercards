﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class GameManager : MonoBehaviour 
{
	private static GameManager _instance = null;
	public static GameManager Get
	{
		get
		{
			if (_instance == null) {
				_instance = FindObjectOfType(typeof(GameManager)) as GameManager;
				return _instance;
			}
			else {
				return _instance;
			}
		}
	}

	public Button endTurn;
	public GameObject gameOverPanel;
	public Text gameOverTitle;

	void Awake()
	{
		DOTween.Init();
		NetworkController.Get.Init();
		endTurn.onClick.AddListener(EndTurnPress);

		StartCoroutine(StartGameCmd());
	}

	IEnumerator StartGameCmd()
	{
		yield return new WaitForSeconds(0.5f);
		new StartGameCmd().Send(OnGameStarted);
	}

	void OnDestroy() 
	{
		NetworkController.Get.Uninit();
	}

	void OnGameStarted(string str)
	{
		
	}

	public void OnLeaveGamePress()
	{
		//SceneManager.LoadScene("mainMenu");
		Application.LoadLevel("mainMenu");
	}

	public void GameOver(bool isWin)
	{
		gameOverPanel.SetActive(true);
		gameOverTitle.text = isWin ? "You win!" : "Defeat";
		endTurn.gameObject.SetActive(false);
	}

	private void EndTurnPress()
	{
		new EndTurnCmd().Send();
	}
}
