﻿using UnityEngine;
using System;
using System.Collections;

public class Slot : MonoBehaviour 
{
	public Action<Slot> onClick;

	void OnMouseDown() {
		if (onClick != null) {
			onClick(this);
		}
	}
}
