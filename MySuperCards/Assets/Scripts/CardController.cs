﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using DG.Tweening;

public class CardController : MonoBehaviour 
{
	public Text attackTxt;
	public Text healthTxt;
	public Text manaTxt;
	public SpriteRenderer bg;
	public Canvas canvas;

	public Action<CardController> onClick;

	public Card card;

	public void Init(Card card, int order)
	{
		this.card = card;
		attackTxt.text = card.attack.ToString();
		healthTxt.text = card.health.ToString();
		manaTxt.text = card.mana.ToString();

		canvas.sortingOrder = order;
		bg.sortingOrder = order;
	}

	public void SetSelected(bool b)
	{
		if (b) {
			bg.color = new Color (1f, 0.8f, 1f);
		} else {
			bg.color = new Color (1f, 1f, 1f);
		}
	}

	void OnMouseDown() {
		if (onClick != null) {
			onClick(this);
		}
	}

	public bool Damage(int damage)
	{
		card.health -= damage;
		if (card.health <= 0) {
			card.health = 0;
			return false;
		}
		healthTxt.text = card.health.ToString();
		return true;
	}

	public IEnumerator Attack(int slot, bool isMy = true)
	{
		Vector3 pos = transform.position;
		Vector3 endPos;
		if (isMy) {
			CardController enemyCard = EnemyTableController.Get.GetCardControllerInSlot(slot);
			if (enemyCard != null) {
				endPos = enemyCard.transform.position;
			} else {
				endPos = EnemyTableController.Get.GetSlotByIndex(slot).transform.position;
			}
		} else {
			CardController enemyCard = TableController.Get.GetCardControllerInSlot(slot);
			if (enemyCard != null) {
				endPos = enemyCard.transform.position;
			} else {
				endPos = TableController.Get.GetSlotByIndex(slot).transform.position;
			}
		}
		Sequence sequence = DOTween.Sequence();
		sequence.Append(transform.DOMove(endPos, 0.15f)).Append(transform.DOMove (pos, 0.15f));
		yield return sequence.WaitForCompletion();
	}
}
