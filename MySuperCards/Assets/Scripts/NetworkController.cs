﻿using UnityEngine;
using System;
using System.Reflection;
using System.Collections;
using SocketIO;

public class NetworkController
{
	private static NetworkController instance;
	public static NetworkController Get
	{
		get {
			if (instance == null) {
				instance = new NetworkController ();
			}
			return instance;
		}
	}



	public void Init()
	{
		Network.Get.Init();
		Network.Get.onReqestCallback += CheckCommands;
	}

	public void Uninit()
	{
		Network.Get.onReqestCallback -= CheckCommands;
	}

	private void CheckCommands(string str)
	{
		JSONObject jsonOblect = new JSONObject(str);
		if (jsonOblect["commands"] != null) {
			foreach (var command in jsonOblect["commands"].list) {
				//JSONObject command = jsonOblect["commands"];
				MethodInfo method = typeof(NetworkController).GetMethod(command["type"].str, BindingFlags.Static | BindingFlags.NonPublic);
				object[] parametersArray = new object[] { command };
				method.Invoke(null, parametersArray);
			}
		}
	}

#region net commands
	private static void UpdateParams(JSONObject param)
	{
		CoroutineTaskManager.Get.AddTask(PlayersController.Get.UpdateParams(param["params"].ToString()));
	}

	private static void GetCardInHands(JSONObject param)
	{
		Card card = JsonUtility.FromJson<Card>(param["card"].ToString());
		CoroutineTaskManager.Get.AddTask(HandController.Get.PutCardInHands(card));
	}

	private static void EnemyPlayCard(JSONObject param)
	{
		int slot = (int)param["slot"].n;
		Card card = JsonUtility.FromJson<Card>(param["card"].ToString());

		CoroutineTaskManager.Get.AddTask(EnemyTableController.Get.PlayCard(card, slot));
	}

	private static void CardAttack(JSONObject param)
	{
		int slot = (int)param["slot"].n;
		int damage = (int)param["damage"].n;

		CoroutineTaskManager.Get.AddTask(CardAttackCoroutine(slot, damage));
	}

	static IEnumerator CardAttackCoroutine(int slot, int damage)
	{
		CardController myCard = TableController.Get.GetCardControllerInSlot(slot);
		yield return CoroutineTaskManager.Get.StartCoroutine(myCard.Attack(slot));

		CardController enemyCard = EnemyTableController.Get.GetCardControllerInSlot(slot);
		if (enemyCard != null) {
			if (!enemyCard.Damage(damage)) { // destroy if die
				yield return new WaitForSeconds(0.2f);
				EnemyTableController.Get.DestroyCard(slot);
			}
		}
		yield return new WaitForSeconds(0.2f);
	}

	private static void EnemyCardAttack(JSONObject param)
	{
		int slot = (int)param["slot"].n;
		int damage = (int)param["damage"].n;

		CoroutineTaskManager.Get.AddTask(EnemyCardAttackCoroutine(slot, damage));
	}

	static IEnumerator EnemyCardAttackCoroutine(int slot, int damage)
	{
		CardController enemyCard = EnemyTableController.Get.GetCardControllerInSlot(slot);
		yield return CoroutineTaskManager.Get.StartCoroutine(enemyCard.Attack(slot, false));

		CardController myCard = TableController.Get.GetCardControllerInSlot(slot);
		if (myCard != null) {
			if (!myCard.Damage(damage)) { // destroy if die
				TableController.Get.DestroyCard(slot);
			}
		}
		yield return new WaitForSeconds(0.2f);
	}

	private static void MatchEnd(JSONObject param)
	{
		string status = param["status"].str;
		Debug.Log ("MATCH END: " + status);
		GameManager.Get.GameOver(status == "win");
	}
#endregion
}
