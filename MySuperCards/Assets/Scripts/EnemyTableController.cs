﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class EnemyTableController : MonoBehaviour {

	public List<Slot> slots;
	public Dictionary<Slot, CardController> cards;

	public GameObject CardPrefab;

	private static EnemyTableController _instance = null;
	public static EnemyTableController Get
	{
		get
		{
			if (_instance == null) {
				_instance = FindObjectOfType(typeof(EnemyTableController)) as EnemyTableController;
				return _instance;
			}
			else {
				return _instance;
			}
		}
	}

	void Awake()
	{
		cards = new Dictionary<Slot, CardController>();
	}

	public IEnumerator PlayCard(Card card, int slotNum)
	{
		Slot slot = slots[slotNum];
		GameObject cardGO = GameObject.Instantiate(CardPrefab, new Vector3(4f, 3f, 0f), slot.transform.rotation) as GameObject;
		CardController cardController = cardGO.GetComponent<CardController>();
		cardController.Init(card, 0);
		Tween tween = cardGO.transform.DOMove(slot.transform.position, 0.4f);
		yield return tween.WaitForCompletion();
		cards.Add(slot, cardController);
	}

	void SlotClicked(Slot slot)
	{
		
	}

	public CardController GetCardControllerInSlot(int slotIndex)
	{
		if (slots.Count > slotIndex) {
			if (cards.ContainsKey(slots[slotIndex])) {
				return cards[slots[slotIndex]];
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public Slot GetSlotByIndex(int slotIndex)
	{
		if (slots.Count > slotIndex) {
			return slots[slotIndex];
		} else {
			return null;
		}
	}

	public void DestroyCard(int slotIndex)
	{
		Slot slot = slots[slotIndex];
		if (cards.ContainsKey(slot)) {
			CardController card = cards[slot];
			cards.Remove(slot);
			Destroy(card.gameObject);
		}
	}
}
